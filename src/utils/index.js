export function shuffle([...arr]) {
  let lengthArray = arr.length;

  while (lengthArray) {
    const i = Math.floor(Math.random() * lengthArray--);
    [arr[lengthArray], arr[i]] = [arr[i], arr[lengthArray]];
  }

  return arr;
}


export function percentOf(val1, val2) {
  return ((val1 * 100) / val2) || 0;
}

export function generateId() {
  return Math.random().toString(36).substr(2, 9);
}
