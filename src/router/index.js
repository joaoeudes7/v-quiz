import Vue from 'vue'
import VueRouter from 'vue-router'
import QuizCollections from '@/views/QuizCollections.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'collections',
    component: QuizCollections
  },
  {
    path: '/quiz/:collectionId',
    name: 'quiz',
    component: () => import(/* webpackChunkName: "quiz" */ '@/views/Quiz.vue')
  },
  {
    path: '/create-quiz',
    name: 'create-quiz',
    component: () => import(/* webpackChunkName: "create-quiz" */ '@/views/CreateQuiz.vue')
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
