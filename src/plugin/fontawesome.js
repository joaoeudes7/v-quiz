import Vue from 'vue'

import { library } from '@fortawesome/fontawesome-svg-core';
import { faArrowRight, faArrowLeft, faCheck, faPlus } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome';

library.add(faArrowRight, faArrowLeft, faCheck, faPlus)

Vue.component('v-icon', FontAwesomeIcon)
