import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

const SET_COLLECTION = 'SET_COLLECTION';
const SET_USER_RESPONSES = 'SET_USER_RESPONSES';
const SAVE_RESPONSE_COLLECTION = 'SAVE_RESPONSE_COLLECTION';
const SAVE_COLLECTION = 'SAVE_COLLECTION';

export default new Vuex.Store({
  state: {
    collections: [],
    userResponses: {}
  },
  mutations: {
    [SET_COLLECTION](state, data) {
      state.collections = data;
    },
    [SET_USER_RESPONSES](state, data) {
      state.userResponses = data;
    },
    [SAVE_RESPONSE_COLLECTION](state, response) {
      state.userResponses[response.collection] = response.questions
    }
  },
  getters: {
    collectionsUnsolved: state => {
      return state.collections.map(c => !state.userResponses[c.name]);
    }
  },
  actions: {
    /**
     * Fetch data default and data of User
     */
    fetchDataQuiz({ commit }) {
      const dataDefault = require('../assets/dataDefault.json');

      const collectionsDefault = dataDefault['collections'];
      const collectionsUser = JSON.parse(localStorage.getItem('collections')) || [];

      commit(SET_COLLECTION, [...collectionsUser, ...collectionsDefault]);
    },

    /**
     * Fetch questions solveds by User
     */
    fetchResponses({ commit }) {
      const responses = JSON.parse(localStorage.getItem('responsesQuiz')) || [];

      commit(SET_USER_RESPONSES, responses);
    },

    /**
     * Save Responses in Store and Local
     * @param {*} responses Object with collection and questions solveds of user
     */
    saveResponses({ commit, state }, responses) {
      commit(SAVE_RESPONSE_COLLECTION, responses);

      localStorage.setItem('responsesQuiz', JSON.stringify(state.userResponses));
    },

    /**
     * Save new Collection
     * @param {*} collection Object with name of collection and question createds
     */
    saveCollection({ commit, state }, collection) {
      const userCollections = JSON.parse(localStorage.getItem('collections')) || [];

      commit(SAVE_COLLECTION, [...state.collections, collection])
      localStorage.setItem(JSON.stringify([...userCollections, collection]))
    }
  }
})
